LISTA USUÁRIO
Funcionalidade: responsável por listar todos os usuários da lista
Url:  #/user/usuarios
Method: GET
Content-Type: application/json
Retorno: 200
Mensagem sucesso: [
    {
        "cpf": 1,
        "nome": "Ana",
        "sobrenome": "Silva",
        "endereco": "rua centro",
        "telefone": 1687691234
    },
    {
        "cpf": 191,
        "nome": "Primeiro",
        "sobrenome": "teste",
        "dataNascimento": “1999-10-10”,
        "endereco": "Rua",
        "telefone": 16991019982
    }
]
Mensagem erro:  {"retorno": “Nenhum usuario encontrado”}

PESQUISA USUÁRIO
Funcionalidade: responsável por buscar o usuário pelo cpf
Url: #/user/usuario/{cpf}
Method: GET
Content-Type: application/json
Retorno: 200
Mensagem sucesso: {
    "cpf": 191,
    "nome": "Primeiro",
    "sobrenome": "teste",
    "dataNascimento": “2017-09-09”,
    "endereco": "Rua",
    "telefone": 16991019982
}
Mensagem erro:  {"retorno": “Usuario nao encontrado”}

REMOVER USUÁRIO
Funcionalidade: responsável por remover o usuário de acordo com o cpf
Url: #/user/delete/{cpf}
Method: POST
Content-Type: application/json
Retorno: 200
Mensagem sucesso: {"retorno": “sucesso”}
Mensagem erro:  {"retorno": “erro”} ou :  {"retorno": “Usuario nao existe”}

ATUALIZAR USUÁRIO
Funcionalidade: responsável por inserir ou atualizar o usuário, se já existir ele altera senão adiciona um novo
Url: #/user/atualizar/
Method: POST
Content-Type: application/json
Body: {
    "cpf": long,
    "nome": String,
    "sobrenome": String
    "dataNascimento": "yyyy-mm-dd",
    "endereco": String,
    "telefone": long
}
Retorno: 200
Mensagem sucesso: {"retorno": “Usuario alterado com sucesso”} ou {"retorno": “Usuario inserido com sucesso”}
Mensagem erro:  {"retorno": “Erro ao atualizar usuario”} ou {"retorno": “Erro ao alterar usuario”}