package com.project.model;

public class Retorno {

	public String retorno;
	
	public Retorno(String retorno) {
		this.retorno = retorno;
	}
	
	public Retorno() {
		this.retorno = "";
	}

	public String getRetorno() {
		return retorno;
	}

	public void setRetorno(String retorno) {
		this.retorno = retorno;
	}
	
}
