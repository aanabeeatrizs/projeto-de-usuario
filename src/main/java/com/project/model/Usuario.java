package com.project.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.project.util.LocalDateDeserializer;

@Entity
@Table(name="usuario")
public class Usuario{
	
	@Id
	@Column(name="cpf")
	@NotNull
	private long cpf;
	@Column(name="nome")
	private String nome;
	@Column(name="sobrenome")
	private String sobrenome;
	@Column(name="datanascimento")
    @Type(type = "java.time.LocalDate")
	
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dataNascimento;
	@Column(name="endereco")
	private String endereco;
	@Column(name="telefone")
	private long telefone;
	
	
	public Usuario(long cpf, String nome, String sobrenome, LocalDate dataNascimento, String endereco, long telefone) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
		this.telefone = telefone;
	}

	public Usuario() {
		super();
		this.cpf = 0;
		this.nome = "";
		this.sobrenome = "";
		this.dataNascimento = null;
		this.endereco = "";
		this.telefone = 0;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getTelefone() {
		return telefone;
	}

}
