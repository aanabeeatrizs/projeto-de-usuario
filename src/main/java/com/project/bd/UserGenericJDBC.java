package com.project.bd;

import java.time.LocalDate;
import java.util.List;

import javax.json.Json;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.model.Usuario;
import com.project.util.LocalDateSerializable;


@RestController
@RequestMapping("/user")
public class UserGenericJDBC  {
	
	@Autowired
	private InterfaceUser user;
	
    @RequestMapping(value = "/usuarios", method = RequestMethod.GET, produces = "application/json")
	public String listar() {
		List<Usuario> lista = user.findAll();
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,new LocalDateSerializable()).create();
		return lista.isEmpty()? Json.createObjectBuilder().add("retorno", "Nenhum usuario encontrado").build().toString():gson.toJson(lista);
	}

    @RequestMapping(value = "/usuario/{cpf}", method = RequestMethod.GET, produces = "application/json")
	public String pesquisaUsuario(@PathVariable long cpf) {
		Usuario usuario = user.findOne(cpf);
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,new LocalDateSerializable()).create();
		return usuario != null ? gson.toJson(usuario): Json.createObjectBuilder().add("retorno", "Usuario nao encontrado").build().toString();
	}
    
    @RequestMapping(value = "/delete/{cpf}", method = RequestMethod.DELETE, produces = "application/json")
   	public String remover(@PathVariable long cpf) {
    	try {
    		if(user.exists(cpf)){
    			user.delete(cpf);
    	    	return Json.createObjectBuilder().add("retorno", "Sucesso").build().toString();
    		}else{
    	    	return Json.createObjectBuilder().add("retorno", "Usuario nao existe").build().toString();
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return Json.createObjectBuilder().add("retorno", "Erro").build().toString();
    }
    
    @ResponseBody
    @RequestMapping(value ={"atualizar"}, method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public String atualizar(@RequestBody Usuario usuario) {
    	try {
    		boolean existe = user.exists(usuario.getCpf());
    		boolean salvo = user.save(usuario) != null;
    		String msg = existe ? (salvo ? "Usuario alterado com sucesso":"Erro ao alterar usuario")
    				:(salvo?"Usuario inserido com sucesso":"Erro ao inserir usuario");
	    	return Json.createObjectBuilder().add("retorno", msg).build().toString();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return Json.createObjectBuilder().add("retorno", "Erro ao atualizar usuario").build().toString();

    }
}
