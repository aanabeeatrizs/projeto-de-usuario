package com.project.bd;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.model.Usuario;

public interface InterfaceUser extends JpaRepository<Usuario, Long> {
}
