package com.project;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.net.HttpURLConnection;
import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.bd.InterfaceUser;
import com.project.model.Retorno;
import com.project.model.Usuario;
import com.project.util.LocalDateSerializable;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class User1ApplicationTests {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private InterfaceUser userbd;
    
	@Test
	public void contextLoads() throws Exception {
		
	}
	
	private void insereUsuario(){
		userbd.save(getUsuario());
	}
	
	private void insereUsuario2(){
		Usuario usu = getUsuario();
		usu.setCpf(123);
		userbd.save(usu);
	}
	
	private Usuario getUsuario(){
		return new Usuario(11111111111L, "Teste", "Usuario", LocalDate.now(), "Rua joao", 1632428976);
	}
	
	@Test
	public void atualizarUsuario() throws Exception {
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,new LocalDateSerializable()).create();
		String body = gson.toJson(getUsuario());
		int cod = this.mockMvc.perform(
	            post("/user/atualizar")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(body))
	            .andReturn().getResponse().getStatus();
		assertTrue(cod == HttpURLConnection.HTTP_OK);
	}
	
	@Test
	public void pesquisaUsuario() throws Exception {
		insereUsuario();
		int cod = this.mockMvc.perform(

	            get("/user/usuario/11111111111")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andReturn().getResponse().getStatus();
		assertTrue(cod == HttpURLConnection.HTTP_OK);
	}
	
	@Test
	public void removerUsuario() throws Exception{
		insereUsuario();
		int cod = this.mockMvc.perform(

	            delete("/user/delete/11111111111")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andReturn().getResponse().getStatus();
		assertTrue(cod == HttpURLConnection.HTTP_OK);
	}
	
	@Test
	public void listarUsuarios() throws Exception{
		insereUsuario();
		insereUsuario2();
		int cod = this.mockMvc.perform(

	            get("/user/usuarios")
	            .contentType(MediaType.APPLICATION_JSON))
	            .andReturn().getResponse().getStatus();
		assertTrue(cod == HttpURLConnection.HTTP_OK);
	}
	
	public void getMsg() {
		System.err.println("Erro");

	}
}
